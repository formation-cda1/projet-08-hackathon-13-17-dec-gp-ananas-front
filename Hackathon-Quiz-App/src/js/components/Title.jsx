import React from "react";
import "../../css/Title.css"

function Title() {
  return (
    <div className="title">
      <h1 id="main-title">
        <span>Quizz</span>
        <span className="br"></span>
        <span id="text-game">Game</span>
      </h1>
    </div>
  );
}

export default Title;
